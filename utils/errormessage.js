
const err = (res, err) => res.status(err.response.status).send(err.response.data);

module.exports = err;