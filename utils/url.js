const STATSPORT = process.env.STATSPORT || 8080;
const BADGESPORT = process.env.BADGESPORT || 3000;

const URL = "http://localhost:";
const STATSURL = `${URL}${STATSPORT}/api`;
const BADGESURL = `${URL}${BADGESPORT}/api`;

module.exports = { STATSURL, BADGESURL };