const express = require("express");
const app = express();

const PORT = process.env.PORT || 8000;


const loginRouter = require("./routers/login");
const scoreRouter = require("./routers/score_router");
const badgeRouter = require("./routers/badge_router");
const moderatorRouter = require("./routers/moderator_router");
const questionRouter = require("./routers/question_router");
const surveyRouter = require("./routers/survey_router");
const technicianRouter = require("./routers/technician_router");
const imageRouter = require("./routers/image_router");

app.use(express.json());
app.use("/api", loginRouter);
app.use("/api", scoreRouter);
app.use("/api", badgeRouter);
app.use("/api", moderatorRouter);
app.use("/api", questionRouter);
app.use("/api", surveyRouter);
app.use("/api", technicianRouter);
app.use("/api", imageRouter);

app.listen(PORT, console.log(`Server is running on port: ${PORT}`));