const express = require("express");
const router = express.Router();
const axios = require("axios");
const url = require("../utils/url");
const STATSURL = url.STATSURL;
const BADGESURL = url.BADGESURL;

const errormessage = require("../utils/errormessage")

router.get("/survey/:id", async (req, res) => {
    const id = req.params.id;
    const params = { params: req.query ? req.query : {} }
    await axios.get(`${STATSURL}/survey/${id}`, params).then(r => res.json(r.data)).catch(err => errormessage(res, err));
})
router.post("/survey", async (req, res) => {
    await axios.post(`${STATSURL}/survey`, req.body).then(r => res.json(r.data)).catch(err => errormessage(res, err));
})
router.put("/surveynoanswer/:id", async (req, res) => {
    await axios.put(`${STATSURL}/surveynoanswer/${req.params.id}`).then(r => res.json(r.data)).catch(err => errormessage(res, err));
})
router.get("/surveys", async (req, res) => {
    await axios.get(`${STATSURL}/surveys`).then(r => res.json(r.data)).catch(err => errormessage(res, err));
})
module.exports = router;