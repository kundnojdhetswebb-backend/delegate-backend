const express = require("express");
const router = express.Router();
const axios = require("axios");
const errormessage = require("../utils/errormessage")
const url = require("../utils/url");
const STATSURL = url.STATSURL;
const BADGESURL = url.BADGESURL;

router.put("/changeLanguageQuestion", async (req, res) => {
    await axios.put(`${STATSURL}/changeLanguageQuestion`, req.body).then(r => res.json(r.data)).catch(err => errormessage(res, err))
});

module.exports = router;