const express = require("express");
const router = express.Router();
const axios = require("axios");
const url = require("../utils/url")

const STATSURL = url.STATSURL;
const BADGESURL = url.BADGESURL;

const errormessage = require("../utils/errormessage");

router.get("/score", async (req, res) => await axios.get(`${STATSURL}/score`).then(r => res.json(r.data)).catch(err => errormessage(res, err)))

router.put("/score", async (req, res) => {
    await axios.put(`${STATSURL}/score`, req.body)
        .then(async (r) => {
            await axios.post(`${BADGESURL}/badge`, {
                t_id: r.data.data.technician.id,
                category: "Survey_Streak"
            });
            res.json(r.data);
        }).catch(err => {
            res.status(err.response.status).send(err.response.data);
        });
});

router.get("/technicianscoreresponse/:id", async (req, res) => {
    const params = { params: req.query ? req.query : {} };
    await axios.get(`${STATSURL}/technicianscoreresponse/${req.params.id}`, params).then(r => res.json(r.data)).catch(err => res.send(err));
});


module.exports = router;