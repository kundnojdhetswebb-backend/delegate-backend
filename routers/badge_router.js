const express = require("express");
const router = express.Router();
const axios = require("axios");
const errormessage = require("../utils/errormessage")
const url = require("../utils/url");
const STATSURL = url.STATSURL;
const BADGESURL = url.BADGESURL;

router.get("/badge/:technicianId", async (req, res) => {
    await axios.get(`${BADGESURL}/badge/${req.params.technicianId}`)
        .then(r => res.json(r.data))
        .catch(err => errormessage(res, err));
});

module.exports = router;