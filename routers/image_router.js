const express = require("express");
const router = express.Router();
const axios = require("axios");
const errormessage = require("../utils/errormessage")
const url = require("../utils/url");
const STATSURL = url.STATSURL;
const BADGESURL = url.BADGESURL;

// router.get("/image/:imageName", async (req, res) => {
//     console.log(req.params.imageName);
//     await axios.get(`${BADGESURL}/image/${req.params.imageName}`, { responseType: 'blob' }).then(r => {
//         console.log(r)
//     }).catch(err => {
//         errormessage(res, err)
//     }
//     );
// })
// router.get("/images", async (req, res) => {
//     await axios.get(`${BADGESURL}/images`).then(r => res.data(r)).catch(err => errormessage(res, err));
// })
// router.get("/image", (req, res) => {
//     res.send("hi")
// })

module.exports = router;