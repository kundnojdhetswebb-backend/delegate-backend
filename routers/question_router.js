const express = require("express");
const router = express.Router();
const axios = require("axios");
const errormessage = require("../utils/errormessage")
const url = require("../utils/url");
const STATSURL = url.STATSURL;
const BADGESURL = url.BADGESURL;

router.get("/questions", async (req, res) => await axios.get(`${STATSURL}/questions`).then(r => res.json(r.data)).catch(err => errormessage(res, err)));

router.post("/question", async (req, res) => await axios.post(`${STATSURL}/question`, req.body).then(r => res.json(r.data)).catch(err => errormessage(res, err)));

module.exports = router;