const express = require("express");
const router = express.Router();
const axios = require("axios");
const url = require("../utils/url");
const errormessage = require("../utils/errormessage");
const BADGESURL = url.BADGESURL;
const STATSURL = url.STATSURL;

router.post("/login", async (req, res) => {
    const id = req.body.t_id; //Change this to the values get by ServiceNow
    let isExist = false;
    await axios.get(`${BADGESURL}/exists/${id}`)
        .then(b => isExist = b.data.data).catch(err => res.status(err.response.status).send("Error while fetching data"));

    if (isExist)
        res.send("User exists, pass to the frontend url");
    else {
        await axios.post(`${BADGESURL}/api/badge`, { t_id: id })
            .then(result => res.send(`A document has been created on t_id: ${id}`))
            .catch(err => {
                res.status(err.response.status).send("Couldnt upload user, check console for log");
            });
    }
});

module.exports = router;