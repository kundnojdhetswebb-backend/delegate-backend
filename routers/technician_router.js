const express = require("express");
const router = express.Router();
const axios = require("axios");

const url = require("../utils/url");
const STATSURL = url.STATSURL;
const BADGESURL = url.BADGESURL;

const errormessage = require("../utils/errormessage");

router.get("/technicians", async (req, res) => axios.get(`${STATSURL}/technicians`).then(r => res.json(r.data)).catch(err => errormessage(res, err)))

router.post("/technician", async (req, res) => {
    await axios.post(`${STATSURL}/technician`, req.body).then(r => res.json(r.data)).catch(err => errormessage(res, err));
})
router.get("/techniciansbyrealm/:id", async (req, res) => {
    const params = { params: req.query ? req.query : {} }
    await axios.get(`${STATSURL}/techniciansbyrealm/${req.params.id}`, params).then(r => res.json(r.data)).catch(err => errormessage(res, err))
})
module.exports = router;