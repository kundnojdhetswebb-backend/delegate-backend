const express = require("express");
const router = express.Router();
const axios = require("axios");
const url = require("../utils/url");
const errormessage = require("../utils/errormessage");

const STATSURL = url.STATSURL;
const BADGESURL = url.BADGESURL;

router.get("/moderators", async (req, res) => await axios.get(`${STATSURL}/moderators`).then(r => res.json(r.data)).catch(err => errormessage(res, err)));

router.get("/moderator/:id", async (req, res) => await axios.get(`${STATSURL}/moderator/${req.params.id}`).then(r => res.json(r.data)).catch(err => errormessage(res, err)));

module.exports = router;